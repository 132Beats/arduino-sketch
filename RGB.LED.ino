int r_pin=9;
int g_pin=10;
int b_pin=3;
int r,g,b;
void setup() {
  pinMode(r_pin,OUTPUT);
  pinMode(g_pin,OUTPUT);
  pinMode(b_pin,OUTPUT);
  r=255;
  g=0;
  b=0;
  analogWrite(r_pin,r);
  analogWrite(g_pin,g);
  analogWrite(b_pin,b);
}

void loop(){
  for(;g<255;g++){
    delay(10);
    analogWrite(g_pin,g);
  }
  for(;r>0;r--){
    delay(10);
    analogWrite(r_pin,r);
  }
  for(;b<255;b++){
    delay(10);
    analogWrite(b_pin,b);
  }
  for(;g>0;g--){
    delay(10);
    analogWrite(g_pin,g);
  }
  for(;r<255;r++){
    delay(10);
    analogWrite(r_pin,r);
  }
  for(;b>0;b--){
    delay(10);
    analogWrite(b_pin,b);
  }
}
