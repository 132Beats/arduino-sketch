#include "SR04.h"
#define TRIG_PIN 12
#define ECHO_PIN 11

int r_pin=9;
int b_pin=3;
unsigned int r,b;
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;
static int j=16;
int aa[16];
int i=0;
int ii=0;

void setup() {
  pinMode(r_pin,OUTPUT);
  pinMode(b_pin,OUTPUT);
  r=0;
  b=255;
  analogWrite(r_pin,r);
  analogWrite(b_pin,b);
}

void loop() {
  a=sr04.Distance();
  if(a>100){
    a=100;
  }
  aa[i]=a;
  if(i<(j-1)){
    i++;
  }
  else{
    i=0;
  }
  for(a=0,ii=0;ii<j;ii++){
    a=a+aa[ii];
  }
  a=a/j;
  a=a*2.55;
  r=255-(a*a/255);
  analogWrite(r_pin,r);
}